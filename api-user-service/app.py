# Import packages
try:
    import datetime
    import requests
    from flask import Flask, request, Response
    from flask_restful import Resource, Api, reqparse
    from mongoengine import connect
    import random
except Exception as e:
    print('Some modules are missing {}'.format(e))


# Models
from models import *


# Add connection string for MongoDB and load with MongoEngine
connect(host="mongodb://mongodb:27017/FlexioBank")


# Define instructions for endpoints with classes
class Countries(Resource):
    def get(self):
        # Create parameters for endpoint
        args_countries_country_id = request.args.get('countryId')
        args_countries_country_iso = request.args.get('countryISO')
        args_countries_country_name = request.args.get('countryName')
        args_countries_currency_iso = request.args.get('currencyISO')
        args_countries_currency_symbol = request.args.get('currencySymbol')
        args_countries_currency_name = request.args.get('currencyName')
        args_countries_language = request.args.get('language')
        args_countries_region = request.args.get('region')

        # Create empty country list
        country_list = []

        # Fail if parameters are missing
        if args_countries_country_id or args_countries_country_iso or args_countries_country_name or args_countries_currency_iso or args_countries_currency_name or args_countries_currency_symbol or args_countries_language or args_countries_region:
            return ("Missing parameters")
        else:
            for country in CountryModel.objects:
                country_list.append(country.countryName)
            return country_list

# Import currencies from database and output as json list
class CurrencyISO(Resource):
    def get(self):
        # Create empty list
        currency_list = []

        # Iterate through objects in Currency collection
        for item in CurrencyModel.objects:
            currency_list.append(item['currencyISO'])
        return currency_list  # Return JSON list of currencies


# Define endpoints for user endpoint
class User(Resource):

    def get(self):
        # Parse paremeter from email
        arg_email = request.args.get('email')

        # Look up email from param
        user_result = UserModel.objects(email=arg_email).to_json()

        # Response
        if UserModel.objects(email=arg_email):
            return Response(user_result, mimetype="application/json", status=200)
        else:
            return Response('User not found', mimetype="application/json", status=404)

    def put(self):
        # Create request parameter
        parser = reqparse.RequestParser()  # Parse params

        # Add parameters for endpoint
        parser.add_argument('userId')
        parser.add_argument('email')
        parser.add_argument('name')
        parser.add_argument('address')
        parser.add_argument('country')
        parser.add_argument('city')
        parser.add_argument('zipcode')
        parser.add_argument('password')

        # Parse arguments to dictionary
        args = parser.parse_args()

        # Map arguments with user model in MongoDB
        user = UserModel(
            userId=args.userId,
            email=args.email,
            password=args.password,
            name=args.name,
            country=args.country,
            city=args.city,
            zipcode=args.zipcode,
            address=args.address,
        )

        # Check if params are entered
        if args.email and args.name and args.password and args.userId:
            # Check if password matches
            if UserModel.objects(password=args.password):
                # Check if user exists
                if UserModel.objects(userId=args.userId).update(userId=args.userId, email=args.email,
                                                                password=args.password, name=args.name,
                                                                country=args.country, city=args.city,
                                                                zipcode=args.zipcode, address=args.address):

                    response = user.save().to_json()
                    return Response("User updated", mimetype="application/json", status=200)
                else:
                    return Response("User doesn't exist", mimetype="application/json", status=404)
            else:
                return Response("Password doesn't match", mimetype="application/json", status=403)
        else:
            return Response("Missing fields: email, name or password", mimetype="application/json", status=400)

    def delete(self):
        delete_arg_email = request.args.get('email')

        if UserModel.objects(email=delete_arg_email):
            return Response(userResult=UserModel.objects(email=delete_arg_email).delete(), mimetype="application/json",
                            status=200)
        else:
            return Response("User doesn't exist", mimetype="application/json", status=404)

# Define endpoint for login
class Login(Resource):
    def get(self):  # GET request for login endpoint
        # Parse arguments
        arg_email = request.args.get('email')
        arg_password = request.args.get('password')

        # Check user is in database
        userresult = UserModel.objects(email=arg_email, password=arg_password).to_json()

        # Error handling and validation
        if arg_email and arg_password:  # Check if arguments are parsed
            if UserModel.objects(email=arg_email):  # Check if user email exists
                if UserModel.objects(password=arg_password):  # Check if password is correct
                    return Response(userresult, mimetype="application/json", status=200)
                else:
                    return Response("Password is incorrect", mimetype="application/json", status=403)
            else:
                return Response("User not found", mimetype="application/json", status=404)
        else:
            return Response("Missing parameters: email or password", mimetype="application/json", status=400)


class Register(Resource):
    def post(self):  # POST request for register endpoint

        parser = reqparse.RequestParser()  # Parse params

        # Add arguments
        parser.add_argument('email')
        parser.add_argument('name')
        parser.add_argument('address')
        parser.add_argument('country')
        parser.add_argument('city')
        parser.add_argument('zipcode')
        parser.add_argument('password')

        random_account = random.randint(1111111111, 9999999999)

        while UserModel.objects(userId=random_account):
            random_account = random.randint(1111111111, 9999999999)
            print(random_account)

        args = parser.parse_args()  # Parse arguments to dictionary

        # Add args to usermodel for MongoEngine
        user = UserModel(
            userId=random_account,
            email=args.email,
            password=args.password,
            name=args.name,
            country=args.country,
            city=args.city,
            zipcode=args.zipcode,
            address=args.address,
            created_datetime=datetime.datetime.now()
        )

        # Check if params are entered
        if args.email and args.name and args.password and random_account:
            # Check if user exists
            if not UserModel.objects(email=user.email):
                return Response(user.save().to_json(), mimetype="application/json", status=200)
            else:
                return Response("User exists", mimetype="application/json", status=403)
        else:
            return Response("Missing fields: email, name or password", mimetype="application/json", status=400)


# Define python API with Flask
app = Flask(__name__)
api = Api(app)

# Register the path and the entities within it


# Define routes
api.add_resource(User, '/user')
api.add_resource(Login, '/login')
api.add_resource(Register, '/register')
api.add_resource(Countries, '/countries')
api.add_resource(CurrencyISO, '/currency_iso')


# Initialize service
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)  # run our Flask app
