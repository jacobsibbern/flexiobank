# Import packages
try:
    import requests, json, datetime
    from flask import Flask, request, Response, jsonify
    from flask_restful import Resource, Api, reqparse
    from mongoengine import connect
except Exception as e:
    print('Some modules are missing {}'.format(e))


# Import database models
from models import *

# MongoDB connection string and database
connect(host='mongodb://mongodb:27017/FlexioBank')


# Define classes to be used as endpoints
class Transactions(Resource):
    def get(self):
        # Parse argument from request
        bank_account_id_arg = request.args.get('bank_account_id')

        # Check if argument is type int
        try:
            int(bank_account_id_arg)
        except ValueError as e:
            return Response('Bank account needs to be type int', mimetype='application/json', status=403)

        # Look up email from param
        response = TransactionModel.objects(fromBankAccountId=bank_account_id_arg).to_json()

        # Response
        if TransactionModel.objects(fromBankAccountId=bank_account_id_arg):
            return Response(response, mimetype='application/json', status=200)
        else:
            return Response('No transactions found for that bank account', mimetype='application/json', status=404)

    def post(self):

        # Make RequestParser available in variable parser
        parser = reqparse.RequestParser()

        # Specify arguments for the HTTP request
        parser.add_argument('from_account')
        parser.add_argument('to_account')
        parser.add_argument('currency')
        parser.add_argument('amount')

        # Parse all arguments to type namespace
        transaction_args = parser.parse_args()

        # Validation
        try:
            float(transaction_args.amount)  # Try to convert to float
        except ValueError as e:  # If amount is not type float then throw exception
            return Response('Amount needs to be type float', mimetype='application/json', status=403)
        try:
            int(transaction_args.to_account)
            int(transaction_args.from_account)
        except ValueError as e:
            return Response('Account fields needs to be type float', mimetype='application/json', status=403)

        # Map arguments with database fields in MongoDB
        transaction = TransactionModel(
            toBankAccountId=transaction_args.to_account,
            fromBankAccountId=transaction_args.from_account,
            currency=transaction_args.currency,
            amount=float(transaction_args.amount),
            created_datetime=datetime.datetime.now()
        )

        # Check if arguments have received an input
        if transaction_args.to_account and transaction_args.from_account and transaction_args.currency and transaction_args.amount:
            # Check if from_account exists
            if BankAccountModel.objects(bankAccountId=transaction_args.from_account):
                # Check if to_account exists
                if BankAccountModel.objects(bankAccountId=transaction_args.to_account):
                    # Check if currency exists
                    if CurrencyModel.objects(currencyISO=transaction_args.currency):
                        try:

                            return Response(transaction.save().to_json(), mimetype='application/json', status=200)
                        except Exception as e:
                            return Response(e, mimetype='application/json', status=500)
                    else:
                        return Response("Currency doesn't exist", mimetype='application/json', status=404)
                else:
                    return Response("to_account doesn't exists", mimetype='application/json', status=404)
            else:
                return Response("from_account doesn't exists", mimetype='application/json', status=404)
        else:
            return Response('Bad request - missing arguments', mimetype='application/json', status=403)


# Define python API with Flask
app = Flask(__name__)
api = Api(app)

# Define routes
api.add_resource(Transactions, '/transactions')

# Initialize service
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8002, debug=True)  # run our Flask app
