try:
    from mongoengine import *
except Exception as e:
    print('Some modules are missing {}'.format(e))

class CountryModel(Document):
    countryId = IntField(required=True, max_length=10, primary_key=True)
    countryISO = StringField(required=False, max_length=10, unique=True)
    countryName = StringField(required=True, max_length=120, unique=True)
    currencyISO = StringField(required=False, max_length=10)
    currencySymbol = StringField(required=False, max_length=10)
    currencyName = StringField(required=False, max_length=120)
    language = StringField(required=False, max_length=50)
    region = StringField(required=False, max_length=70)


class CurrencyModel(Document):
    currencyISO = StringField(required=True, max_length=5, primary_key=True)
    currencyISOValue = DecimalField(required=False, max_length=20)
    updated_datetime = DateTimeField(required=False, max_length=70)


class UserModel(Document):
    userId = IntField(required=True, primary_key=True, max_length=10)
    email = StringField(required=True, unique=True, max_length=70)
    name = StringField(required=False, max_length=70)
    address = StringField(required=False, max_length=70)
    country = StringField(required=False, max_length=70)
    city = StringField(required=False, max_length=70)
    zipcode = StringField(required=False, max_length=10)
    password = StringField(required=True, max_length=70)
    created_datetime = DateTimeField(required=False, max_length=70)


class BankAccountModel(Document):
    bankAccountId = IntField(required=True, max_length=70, primary_key=True)
    bankAccountName = StringField(Required=True, max_length=30)
    main_currency = ReferenceField(CurrencyModel, required=True, max_length=3)
    userId = ReferenceField(UserModel, reverse_delete_rule=CASCADE)
    balance = DecimalField(required=True, default=0)
    created_datetime = DateTimeField(required=True, max_length=70)

class CurrencyModel(Document):
    currencyISO = StringField(required=True, max_length=5, primary_key=True)
    currencyISOValue = DecimalField(required=False, max_length=20)
    updated_datetime = DateTimeField(required=False, max_length=70)
