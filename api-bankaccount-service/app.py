# Import packages
try:
    import json
    import datetime
    import urllib.request
    import requests
    from flask import Flask, request, Response
    from flask_restful import Resource, Api, reqparse
    from mongoengine import connect
    import random
except Exception as e:
    print('Some modules are missing {}'.format(e))


    # Models
from models import *


CURRENCIES_API_URL = 'https://v6.exchangerate-api.com/v6/b74bd121f9bef7f2c50201ca/latest'


# Add connection string for MongoDB and load with MongoEngine
connect(host="mongodb://mongodb:27017/FlexioBank")


# def calculate_exchange_rate(from_currency, to_currency, amount):
#     # Import currencies from public API
#     response = requests.get(CURRENCIES_API_URL + '/' + from_currency)
#
#     # Convert to dict
#     currency_dict = response.json()
#
#     # Extract rates from dict
#     rates = currency_dict['conversion_rates']
#
#     for item in CurrencyModel.objects(currencyISO=from_currency):
#         from_currency_value = item.currencyISOValue
#         print(from_currency_value)
#
#     for item in CurrencyModel.objects(currencyISO=to_currency):
#         to_currency_value = item.currencyISOValue
#         print(to_currency_value)
#
#     # (50 * DKK)
#
#
#     print(
#         amount*(from_currency_value*to_currency_value)
#     )
#
#
# #calculate_exchange_rate('EUR', 'DKK', 1000)


class BankAccount(Resource):
    def get(self):

        arg_userid = request.args.get('userId')

        # Look up email from param
        userresult = BankAccountModel.objects(userId=arg_userid).to_json()

        # Response
        if BankAccountModel.objects(userId=arg_userid):
            return Response(userresult, mimetype="application/json", status=200)
        else:
            return Response("User doesn't have any bank accounts", mimetype="application/json", status=404)

    def post(self):

        parser = reqparse.RequestParser()

        parser.add_argument('bankAccountName')
        parser.add_argument('main_currency')
        parser.add_argument('userId')
        parser.add_argument('balance')

        bankaccount_args = parser.parse_args()

        random_account = random.randint(1111111111, 9999999999)

        bankaccount = BankAccountModel(
            bankAccountId=random_account,
            bankAccountName=bankaccount_args.bankAccountName,
            main_currency=bankaccount_args.main_currency,
            userId=bankaccount_args.userId,
            balance=bankaccount_args.balance,
            created_datetime=datetime.datetime.now()
        )

        if bankaccount_args.userId and bankaccount_args.main_currency:
            # Check if account exists
            if not BankAccountModel.objects(bankAccountId=bankaccount.bankAccountId):
                return Response(bankaccount.save().to_json(), mimetype="application/json", status=200)
            else:
                return Response("Account exists", mimetype="application/json", status=403)

        else:
            return Response("Missing required fields: accountId, main_currency or created_datetime",
                            mimetype="application/json", status=400)

    def put(self):
        arg_bank_account_id = request.args.get('bank_account_id')
        arg_currency = request.args.get('currency_iso')
        arg_amount = request.args.get('amount')

        try:
            float(arg_amount)
        except ValueError:
            return Response('Amount is not of type float',mimetype='application/json', status=403)



        for bank_account in BankAccountModel.objects(bankAccountId=arg_bank_account_id):
            new_balance = float(bank_account.balance) + float(arg_amount)

        BankAccountModel(bankAccountId=arg_bank_account_id).update(balance=(new_balance))
        return 'Balance updated'

    def delete(self):
        delete_bank_accountId = int(request.args.get('bankAccountId'))

        if BankAccountModel.objects(bankAccountId=delete_bank_accountId):
            BankAccountModel.objects(bankAccountId=delete_bank_accountId).delete()
            return(Response('Bank account deleted', status = 200))
        else:
            return Response("Bank account doesn't exist", mimetype="application/json", status=404)


# Define python API with Flask
app = Flask(__name__)
api = Api(app)

# Register the path and the entities within it


# Define routes
api.add_resource(BankAccount, '/bankaccount')

# Initialize service
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8003, debug=False)  # run our Flask app
