# FlexioBank
## About
- **Author:** KEA - BEIT 2019 DA | Group 2 | Jacob Sibbern, Jonathan Arge, Thomas Jørgsholm, Valdemar Olesen
- **Purpose:** Developed for a school delivery.

Demonstration of a 3-layered architecture developed in Python for front-end client and back-end API's using Flask.  
The database is run using docker thanks to bitnami/mongodb docker image that has been added to the docker-compose file. In production this would be hosted in a datacenter. 

This project is divided up in microservices. Each sub-folder in the root folder is a microservice intented to run as a docker container. 

If you follow the installation guide below it should run smoothly. This project has been tested on Mac OS X / Windows and several IDE's such as Visual Studio Code and PyCharm.

## Requirements
### Services:
- Install Docker - https://docs.docker.com/engine/install/
- Install Docker-Compose - https://docs.docker.com/compose/install/
  
### Front-end client:
- Install Git - https://git-scm.com/downloads
- Install Python version 3.9 (recommended) or 3.8 - https://www.python.org/downloads/
- Install virtualenv for python - ```pip install virtualenv```

## Installation

#### 1: Open a terminal or commandprompt and go to a directory suited for the project. Future commands will be expected to run in that terminal or commandprompt


#### 2: Download the source code with git:
```
git clone https://gitlab.com/jacobsibbern/flexiobank.git
```

##### 3: Change to the project directory:
```
cd ./flexiobank
```
#### 4: Make sure the docker-compose.yml file is in the current directory and start all docker containers up with docker-compose:
```
docker-compose up -d
```
This might take some time as it's pulling all the docker images from [docker.io](https://hub.docker.com/repositories/jacobsibbern). 

#### 5: Create a virtual python environment (See [link](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/)):
###### Windows
```
py -m venv bank-system-service-env
cd \bank-system-service-env\Scripts\
activate
```

###### Linux / Mac OS
```
python3 -m venv bank-system-service-env
cd bank-system-service-env bank-system-service-env $ source bin/activate
```

##### 6: Go back to the project directory where flexiobank was located and change directory to python-client:

###### Windows
```
cd .\python-client
```

###### Linux / Mac OS
```
cd ./python-client
```


#### 6: Install dependency packages
```
pip install -r requirements.txt
```


#### 7: Run the front-end Python client 
###### Windows
```
cd "C:\to\directory\for\flexiobank"
py app.py
```

###### Linux / Mac OS
```
cd /to/directory/for/flexiobank/
python3 app.py
```

## Troubleshooting
- Check console output. If it fails to start it will tell you why.
- Check if all API's are running and connected. Use a browser and enter the URL for each service found in the config.py file in python-client. **NOTE: A 404 not found means it's running!**.
- Go through the requirements.txt and manually install the modules to see if all modules are installed successfully.
