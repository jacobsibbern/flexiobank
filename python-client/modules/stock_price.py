# Import 3rd party libraries
try:
    import math
    import matplotlib.pyplot as plt
    import pandas_datareader as web
    import numpy as np
    import pandas as pd
    from sklearn.preprocessing import MinMaxScaler
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.layers import Dense, LSTM
except Exception as e:
    print('Some modules are missing {}'.format(e))

# Use theme for matplotlib

plt.style.use('fivethirtyeight')


def get_company_stock(stock_company):

    # Get stock and convert to dataframe 

    df = web.DataReader(
        stock_company,
        data_source='yahoo',
        start='2012-01-01',
        end='2021-4-26'
    )
    df

    # Visualize closing price history

    plt.figure(figsize=(16,8))
    plt.title('Close Price History of ' + stock_company)
    plt.plot(df['Close'])
    plt.xlabel('Date', fontsize=18)
    plt.ylabel('Close Price USD', fontsize=18)
    plt.show()

    # Create a new dataframe with close column
    data = df.filter(['Close'])

    # Convert dataframe to numpy array
    dataset = data.values

    # Get number of rows to train
    training_data_len = math.ceil( len(dataset) * .8)

    training_data_len

    # Scale the dataa
    scaler = MinMaxScaler(feature_range=(0,1))
    scaled_data = scaler.fit_transform(dataset)

    # Create training data set
    # Create scaled training data set
    train_data = scaled_data[0:training_data_len, :]

    # Split data into x_train and y_train data sets
    x_train = []
    y_train = []

    for i in range(60, len(train_data)):
        x_train.append(train_data[i-60:i])
        y_train.append(train_data[i,0])

    # Convert x_train and y_train to numpy array
    x_train, y_train = np.array(x_train), np.array(y_train)

    # Reshape data
    x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1],1))

    # Build LTSM model
    model = Sequential()
    model.add(LSTM(50, return_sequences=True, input_shape = (x_train.shape[1],1)))
    model.add(LSTM(50, return_sequences=True))
    model.add(LSTM(50, return_sequences=False))
    model.add(Dense(25))
    model.add(Dense(1))

    # Compile model
    model.compile(optimizer='adam', loss='mean_squared_error')

    # Train
    model.fit(x_train, y_train, batch_size=64, epochs=10)

    # Create testing data set
    # Create new array with scaled values from index 1543 to 2003
    test_data = scaled_data[training_data_len - 60 :]

    # Create data sets x_test and y_test

    x_test = []
    y_test = dataset[training_data_len:, :]

    for i in range(60, len(test_data)):
        x_test.append(test_data[i-60:i, 0])
        
    # Convert data to numpy array
    x_test = np.array(x_test)

    # Reshape the data
    x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

    # Get models predicted price values
    predictions = model.predict(x_test)
    predictions = scaler.inverse_transform(predictions)

    # Get the root mean squared error (RMSE)
    rmse = np.sqrt( np.mean( predictions - y_test)**2)
    print(rmse)

    train = data[:training_data_len]
    valid = data[training_data_len:]
    valid['Predictions'] = predictions

    # Visualize data
    plt.figure(figsize=(16,8))
    plt.title(stock_company)
    plt.xlabel('Date', fontsize=18)
    plt.ylabel('Close Price USD ($)', fontsize=18)
    plt.plot(train['Close'])
    plt.plot(valid[['Close', 'Predictions']])
    plt.legend(['Training', 'Validations', 'Predictions'], loc='lower right')
    plt.show()