# Import modules
from config import *

# Import transfer screen
from modules.transfer import *

# Helpers

# Check if datbase is populated with master data
def initiate_datbase():
    countries_response = requests.get(API_IMPORT_SERVICE + '/import_countries')
    print(countries_response.text)

    currencies_response = requests.get(API_IMPORT_SERVICE + '/import_currencies')
    print(currencies_response.text)

initiate_datbase()

# Import stock prediction
def get_stock_prediction(stock):
    # Import modules
    from modules.stock_price import get_company_stock

    # Call function with parameter
    get_company_stock(stock)


# Get all countries in a list
def get_countries():
    global COUNTRIES
    COUNTRIES = []
    response = requests.get(API_USER_SERVICE + "/countries")
    country_json_array = json.loads(response.text)

    # Iterate through json array and append list
    for item in country_json_array:
        COUNTRIES.append(item)


# Get all currencies in a list
def get_currencies():
    global CURRENCIES
    CURRENCIES = []
    response = requests.get(API_USER_SERVICE + "/currency_iso")
    currency_json = json.loads(response.text)

    # Iterate through json array and append list
    for item in currency_json:
        CURRENCIES.append(item)


# Define dynamic message window
def message(title, text, button_text, command, screen):
    global message_screen
    message_screen = Toplevel(screen)
    message_screen.title(title)
    message_screen.configure(bg=PRIMARY_BG)
    message_screen.iconbitmap(FAVICON_PATH)
    message_screen.geometry(SCREEN_SIZE_S)
    Label(message_screen, text=text, bg=PRIMARY_BG, fg=PRIMARY_FG, font=H4).pack()
    Button(message_screen, text=button_text, bg=SECONDARY_BG_BTN, fg=SECONDARY_FG, font=H3, width=15, relief=RAISED,
           height=1, command=command).pack(fill=BOTH, padx=10, pady=20)


# GET request to get users bank account from API service
def get_bank_accounts(self, user_id):
    global BANKACCOUNTS
    BANKACCOUNTS = []
    response_bank_accounts = requests.get(API_BANKACCOUNT_SERVICE + "/bankaccount", params={
        'userId': user_id,
    })

    # Load to JSON
    bankaccounts_json_array = json.loads(response_bank_accounts.text)

    for item in bankaccounts_json_array:
        BANKACCOUNTS.append(item['_id'])

    # Output list of bank accounts for user
    return BANKACCOUNTS


# Design tkinter windows


# Create bank account screen
def create_bank_account(user_id):
    # Define settings for a window
    global create_bank_account_screen
    create_bank_account_screen = Toplevel()
    create_bank_account_screen.title(COMPANY_NAME + " - " + "Login")
    create_bank_account_screen.geometry(SCREEN_SIZE_LLM)
    create_bank_account_screen.iconbitmap(FAVICON_PATH)
    create_bank_account_screen.configure(bg=PRIMARY_BG)

    # Define global variables for window
    global bankAccountName
    global main_currency
    global balance

    # Tkinter requires entries to be global variables
    global bankAccountName_entry
    global main_currency_entry
    global balance_entry

    # Define datatype for variables
    bankAccountName = StringVar()
    main_currency = StringVar()
    balance = IntVar()

    # Label and entry for bank account name
    Label(create_bank_account_screen, text="Please enter the details below", fg=PRIMARY_FG, bg=PRIMARY_BG).pack()
    Label(create_bank_account_screen, text="", fg=PRIMARY_FG, bg=PRIMARY_BG).pack()
    bank_account_name_label = Label(create_bank_account_screen, text="Bank account name * ", fg=PRIMARY_FG, bg=PRIMARY_BG)
    bank_account_name_label.pack()
    bank_account_name_entry = Entry(create_bank_account_screen, textvariable=bankAccountName, fg=PRIMARY_FG, bg=PRIMARY_BG)
    bank_account_name_entry.pack()

    # Get list of currencies to display in dropdown
    get_currencies()

    # Dropdown for currency list
    main_currency_label = Label(create_bank_account_screen, text="Main currency: * ", fg=PRIMARY_FG, bg=PRIMARY_BG)
    main_currency_label.pack()
    main_currency = StringVar(create_bank_account_screen)
    main_currency.set("DKK")
    main_currency_entry = OptionMenu(create_bank_account_screen, main_currency, *CURRENCIES)
    main_currency_entry.pack()

    # Should be turned off in none development environment
    balance_label = Label(create_bank_account_screen, text="Balance: * ", fg=PRIMARY_FG, bg=PRIMARY_BG)
    balance_label.pack()
    balance_entry = Entry(create_bank_account_screen, textvariable=balance)
    balance_entry.pack()

    # Button for calling create bank account function
    Button(
        create_bank_account_screen,
        text="Create Bank Account",
        bg=SECONDARY_BG_BTN,
        fg=SECONDARY_FG,
        font=H3,
        width=15,
        relief=RAISED,
        height=1,
        command=lambda: submit_create_bank_account(
            bankAccountName.get(),
            main_currency.get(),
            user_id,
            balance.get()
        )
    ).pack()


# Call back-end with parameters from input
def submit_create_bank_account(bank_account_name, main_currency, user_id, balance):
    response = requests.post(API_BANKACCOUNT_SERVICE + '/bankaccount', params={
        'bankAccountName': bank_account_name,
        'main_currency': main_currency,
        'userId': user_id,
        'balance': balance
    })
    # Error handling
    if response.status_code == 200:
        message("Success", "Bank account created.", "Close", close_message, create_bank_account_screen)
        close_settings()
        close_dashboard_screen()
        user1.main_account_screen()
    else:
        message("Error", response.text, "Close", close_message, create_bank_account_screen)

# Define class for all functions regarding Transfer
class TransferArg(object):
    def __init__(self):  # Init class and attributes
        self.user_id = ""
        self.name = ""
        self.selected_bank_account = ""

        global transfer_screen
        global BANKACCOUNTS

    # Window to view all transactions for a given bank account
    def see_transactions(self,selected_bank_account): #
        see_transactions_screen = Tk()
        see_transactions_screen.title(COMPANY_NAME + " - " + "Transactions")
        see_transactions_screen.geometry(SCREEN_SIZE_L)
        see_transactions_screen.iconbitmap(FAVICON_PATH)
        see_transactions_screen.configure(bg=PRIMARY_BG)

        # Get transactions for a selected bank account
        response = requests.get(API_TRANSACTION_SERVICE + '/transactions', params={
            'bank_account_id': selected_bank_account.get()
        })

        dict_response = json.loads(response.text)

        # Create empty list for table
        converted_list = []
        # Add headers
        converted_list.append(['Datetime', 'From Bank Account ID', 'To Bank Account ID', 'Currency', 'Amount'])

        # Iterate through dictionary and append list to display as a table
        for item in dict_response:
            from_bank_account_item = item['fromBankAccountId']
            to_bank_account_item = item['toBankAccountId']
            currency_item = item['currency']
            amount_item = item['amount']
            created_datetime = item['created_datetime']['$date']
            converted_list.append([created_datetime, from_bank_account_item, to_bank_account_item, currency_item, amount_item])

        # Get list length
        total_rows = len(converted_list)
        total_columns = len(converted_list[0])

        # code for creating table
        for i in range(total_rows):
            for j in range(total_columns):
                self.e = Entry(see_transactions_screen, width=20, fg=PRIMARY_FG,
                               font=(PRIMARY_TEXT))

                self.e.grid(row=i, column=j)
                self.e.insert(END, converted_list[i][j])

    # Transfer window (Not finished)
    def transfer(self, user_id, name, selected_bank_account):
        transfer_screen = Tk()
        transfer_screen.title(COMPANY_NAME + " - " + "Transfer")
        transfer_screen.geometry(SCREEN_SIZE_LM)
        transfer_screen.iconbitmap(FAVICON_PATH)
        transfer_screen.configure(bg=PRIMARY_BG)

        Label(transfer_screen, text="Transfer from", bg=PRIMARY_BG, fg=PRIMARY_FG, font=H1).pack()

        transfer_screen_label = Label(transfer_screen, text="Selected Bank Account: ", fg=PRIMARY_FG, bg=PRIMARY_BG, font=H2)
        transfer_screen_label.pack()
        selected_bank_account = StringVar(transfer_screen)
        selected_bank_account.set(BANKACCOUNTS[0])
        transfer_screen_entry = OptionMenu(transfer_screen, selected_bank_account, *BANKACCOUNTS)
        transfer_screen_entry.pack()

        # Button to view transactions for a bank account
        Button(
            transfer_screen,
            text="See Transactions",
            command=lambda: transfer1.see_transactions(selected_bank_account),
            background=SECONDARY_BG_BTN,
            fg=SECONDARY_FG,
            font=H3,
            bd=0
        ).pack(padx=10, pady=10, expand=True)


# Create user class with constant attributes
class UserArg(object):
    def __init__(self):  # Init class
        self.user_id = ""
        self.name = ""
        self.address = ""
        self.country = ""
        self.city = ""
        self.zipcode = ""
        self.email = ""

        global main_screen

    # Designing register window
    def register(self):
        global register_screen
        register_screen = Toplevel()
        register_screen.title(COMPANY_NAME + " - " + "Register")
        register_screen.geometry(SCREEN_SIZE_LLM)
        register_screen.iconbitmap(FAVICON_PATH)
        register_screen.configure(bg=PRIMARY_BG)

        get_countries()

        global email
        global password
        global name
        global country
        global city
        global zipcode
        global address
        global new_country

        global email_entry
        global password_entry
        global fullname_entry
        global country_entry
        global city_entry
        global zipcode_entry
        global address_entry

        email = StringVar()
        password = StringVar()
        name = StringVar()
        country = StringVar()
        city = StringVar()
        zipcode = StringVar()
        address = StringVar()

        Label(register_screen, text="Please enter details below", fg=PRIMARY_FG, bg=PRIMARY_BG).pack()
        Label(register_screen, text="", fg=PRIMARY_FG, bg=PRIMARY_BG).pack()
        email_lable = Label(register_screen, text="Email * ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        email_lable.pack()
        email_entry = Entry(register_screen, textvariable=email, fg=PRIMARY_FG, bg=PRIMARY_BG)
        email_entry.pack()

        fullname_lable = Label(register_screen, text="name * ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        fullname_lable.pack()
        fullname_entry = Entry(register_screen, textvariable=name)
        fullname_entry.pack()

        password_lable = Label(register_screen, text="Password * ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        password_lable.pack()
        password_entry = Entry(register_screen, textvariable=password, show='*')
        password_entry.pack()

        country_lable = Label(register_screen, text="Country", fg=PRIMARY_FG, bg=PRIMARY_BG)
        country_lable.pack()
        new_country = StringVar(register_screen)
        new_country.set("Denmark")
        country_entry = OptionMenu(register_screen, country, *COUNTRIES)
        country_entry.pack()

        city_lable = Label(register_screen, text="City", fg=PRIMARY_FG, bg=PRIMARY_BG)
        city_lable.pack()
        city_entry = Entry(register_screen, textvariable=city)
        city_entry.pack()

        zipcode_lable = Label(register_screen, text="Zipcode", fg=PRIMARY_FG, bg=PRIMARY_BG)
        zipcode_lable.pack()
        zipcode_entry = Entry(register_screen, textvariable=zipcode)
        zipcode_entry.pack()

        address_lable = Label(register_screen, text="Address", fg=PRIMARY_FG, bg=PRIMARY_BG)
        address_lable.pack()
        address_entry = Entry(register_screen, textvariable=address)
        address_entry.pack()

        Label(register_screen, text="").pack()
        Button(
            register_screen,
            text="Register",
            bg=SECONDARY_BG_BTN,
            fg=SECONDARY_FG,
            font=H3,
            width=15,
            relief=RAISED,
            height=1,
            command=user1.register_user
        ).pack(fill=BOTH, padx=10, pady=20)


    # Designing window for login
    def login(self):
        global login_screen
        login_screen = Toplevel(main_screen)
        login_screen.title(COMPANY_NAME + " - " + "Login")
        login_screen.geometry(SCREEN_SIZE_M)
        login_screen.iconbitmap(FAVICON_PATH)
        login_screen.configure(bg=PRIMARY_BG)

        global email_verify
        global password_verify

        email_verify = StringVar()
        password_verify = StringVar()

        global email_login_entry
        global password_login_entry

        Label(login_screen, text="", bg=PRIMARY_BG, fg=PRIMARY_FG, font=H3).pack()
        Label(login_screen, text="Email *", bg=PRIMARY_BG, fg=PRIMARY_FG, font=H3).pack()
        email_login_entry = Entry(login_screen, textvariable=email_verify, width=30, bd=2, bg=PRIMARY_BG)
        email_login_entry.pack()
        Label(login_screen, text="Password *", bg=PRIMARY_BG, fg=PRIMARY_FG, font=H3).pack()
        password_login_entry = Entry(login_screen, textvariable=password_verify, show='*', width=30, bd=2)
        password_login_entry.pack()
        Button(login_screen, text="Login", bg=SECONDARY_BG_BTN, fg=SECONDARY_FG, font=H3, width=15, relief=RAISED,
               height=1, command=lambda: user1.login_verify()).pack(fill=BOTH, padx=10, pady=20)


    # Implementing event on register button
    def register_user(self):

        email_info = email.get()
        password_info = password.get()
        fullname_info = name.get()
        country_info = new_country.get()
        city_info = city.get()
        zipcode_info = zipcode.get()
        address_info = address.get()

        response = requests.post(API_USER_SERVICE + '/register', params={
            'email': email_info,
            'password': password_info,
            'name': fullname_info,
            'country': country_info,
            'city': city_info,
            'zipcode': zipcode_info,
            'address': address_info
        })

        if response.status_code == 200:

            user_response = requests.get(API_USER_SERVICE + '/user', params={
                'email': email_info
            })

            json_array = json.loads(user_response.text)
            # print(json_array)

            for item in json_array:
                user_id = item['_id']

                requests.post(API_BANKACCOUNT_SERVICE + '/bankaccount', params={
                    'bankAccountName': 'Default',
                    'main_currency': 'DKK',
                    'userId': user_id,
                    'balance': 0
                })

                message("Success", "User created. Return to the main screen and login", "Close", close_register, register_screen)

        else:
            message("Error", response.text, "Close",close_message, register_screen)

        email_entry.delete(0, END)
        password_entry.delete(0, END)


    # Implementing event on login button
    def login_verify(self):
        email1 = email_verify.get()
        password1 = password_verify.get()
        email_login_entry.delete(0, END)
        password_login_entry.delete(0, END)

        response = requests.get(API_USER_SERVICE + '/login', params={'email': email1, 'password': password1})

        if response.status_code == 200:  # Error handling
            login_screen.destroy()
            main_screen.destroy()

            json_array = json.loads(response.text)
            # print(json_array)

            for item in json_array:
                user_id = item['_id']
                user_email = item['email']
                user_name = item['name']
                user_address = item['address']
                user_country = item['country']
                user_city = item['city']
                user_zipcode = item['zipcode']

            user1.dashboard(user_id, user_name, user_address, user_country, user_city, user_zipcode, user_email)

        else:
            message("Error", response.text, "Close", close_message, login_screen)


    # Create function to update user with new settings
    def update_user(self, user_id, name, address, country, city, zipcode, email):
        response = requests.put(API_USER_SERVICE + '/user', params={
            'userId': user_id,
            'email': new_email.get(),
            'name': new_name.get(),
            'address': new_address.get(),
            'country': new_country.get(),
            'city': new_city.get(),
            'zipcode': new_zipcode.get(),
            'password': password.get()
        })

        if response.status_code == 200:
            message(title="Success", text=response.text, button_text="Close", command=close_message,
                    screen=dashboard_screen)
            close_dashboard_screen()
            user1.main_account_screen()
        else:
            message(title="Error", text=response.text, button_text="Close", command=close_message,
                    screen=settings_screen)


    # Design settings window
    def settings(self, user_id, name, address, country, city, zipcode, email):
        global new_email
        global password
        global new_name
        global new_country
        global new_city
        global new_zipcode
        global new_address

        global email_settings_entry
        global password_settings_entry
        global name_settings_entry
        global country_settings_entry
        global city_settings_entry
        global zipcode_settings_entry
        global address_settings_entry

        global settings_screen
        settings_screen = Toplevel()
        settings_screen.title(COMPANY_NAME + " - " + "Settings")
        settings_screen.geometry(SCREEN_SIZE_L)
        settings_screen.configure(bg=PRIMARY_BG)
        settings_screen.iconbitmap(FAVICON_PATH)

        get_countries()

        Label(settings_screen, text="Account Settings " + name, bg=PRIMARY_BG, fg=PRIMARY_FG, font=H1).grid(row=0,
                                                                                                            column=2)
        Label(settings_screen, text="Personal Settings", bg=PRIMARY_BG, fg=PRIMARY_FG, font=(H2)).grid(row=1, column=1,
                                                                                                       rowspan=2)
        # Personal settings
        Label(settings_screen, fg=PRIMARY_FG, bg=PRIMARY_BG, text="User ID: " + str(user_id)).grid(row=3, column=1)

        new_name = StringVar()
        new_address = StringVar()
        new_city = StringVar()
        new_country = StringVar()
        new_email = StringVar()
        new_zipcode = StringVar()
        password = StringVar()

        email_settings_lable = Label(settings_screen, text="Current email: " + email, fg=PRIMARY_FG, bg=PRIMARY_BG)
        email_settings_lable.grid(row=4, column=1)
        email_new_settings_lable = Label(settings_screen, text="New email: ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        email_new_settings_lable.grid(row=4, column=2)
        email_settings_entry = Entry(settings_screen, textvariable=new_email, fg=PRIMARY_FG, bg=PRIMARY_BG)
        email_settings_entry.grid(row=4, column=3)

        name_settings_lable = Label(settings_screen, text="Current name: " + name, fg=PRIMARY_FG, bg=PRIMARY_BG)
        name_settings_lable.grid(row=5, column=1)
        name_new_settings_lable = Label(settings_screen, text="New name: ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        name_new_settings_lable.grid(row=5, column=2)
        name_settings_entry = Entry(settings_screen, textvariable=new_name)
        name_settings_entry.grid(row=5, column=3)

        country_settings_lable = Label(settings_screen, text="Country: " + country, fg=PRIMARY_FG, bg=PRIMARY_BG)
        country_settings_lable.grid(row=6, column=1)
        country_new_settings_lable = Label(settings_screen, text="New country: ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        country_new_settings_lable.grid(row=6, column=2)
        new_country.set("")
        country_settings_entry = OptionMenu(settings_screen, new_country, *COUNTRIES)
        country_settings_entry.grid(row=6, column=3)

        city_settings_lable = Label(settings_screen, text="City: " + city, fg=PRIMARY_FG, bg=PRIMARY_BG)
        city_settings_lable.grid(row=7, column=1)
        city_new_settings_lable = Label(settings_screen, text="New city: ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        city_new_settings_lable.grid(row=7, column=2)
        city_settings_entry = Entry(settings_screen, textvariable=new_city)
        city_settings_entry.grid(row=7, column=3)

        zipcode_settings_lable = Label(settings_screen, text="Zipcode: " + zipcode, fg=PRIMARY_FG, bg=PRIMARY_BG)
        zipcode_settings_lable.grid(row=8, column=1)
        zipcode_new_settings_lable = Label(settings_screen, text="New zipcode: ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        zipcode_new_settings_lable.grid(row=8, column=2)
        zipcode_settings_entry = Entry(settings_screen, textvariable=new_zipcode)
        zipcode_settings_entry.grid(row=8, column=3)

        address_settings_lable = Label(settings_screen, text="Address: " + address, fg=PRIMARY_FG, bg=PRIMARY_BG)
        address_settings_lable.grid(row=9, column=1)
        address_new_settings_lable = Label(settings_screen, text="New address: ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        address_new_settings_lable.grid(row=9, column=2)
        address_settings_entry = Entry(settings_screen, textvariable=new_address)
        address_settings_entry.grid(row=9, column=3)

        password_settings_lable = Label(settings_screen, text="Current password * ", fg=PRIMARY_FG, bg=PRIMARY_BG)
        password_settings_lable.grid(row=10, column=2)
        password_settings_entry = Entry(settings_screen, textvariable=password)
        password_settings_entry.grid(row=10, column=3)

        command_var = lambda: user1.update_user(user_id, new_name.get(), new_address.get(), new_country.get(), new_city.get(), new_zipcode.get(), new_email.get())
        Button(settings_screen,text="Update",bg=SECONDARY_BG_BTN,fg=SECONDARY_FG,font=H3,width=15,relief=RAISED,height=1,
            command=command_var).grid(row=11, column=3)

        # Bank account settings

        Label(settings_screen, text="Bank Account Settings", bg=PRIMARY_BG, fg=PRIMARY_FG, font=(H2)).grid(row=12, column=1)

        Button(settings_screen, command=lambda: create_bank_account(user_id), text="Create Bank Account",bg=SUCCESS_BTN, fg=SECONDARY_FG, font=H2).grid(row=13, column=1, pady=5)


    # Design dashboard when logged in
    def dashboard(self, user_id, name, address, country, city, zipcode, email):
        global dashboard_screen
        dashboard_screen = Tk()
        dashboard_screen.title(COMPANY_NAME + " - " + "Dashboard")
        dashboard_screen.geometry(SCREEN_SIZE_L)
        dashboard_screen.configure(bg=PRIMARY_BG)
        dashboard_screen.iconbitmap(FAVICON_PATH)

        # Welcome title
        Label(text="Welcome " + name, bg=PRIMARY_BG, fg=PRIMARY_FG, font=H1).pack()

        # Get selected bank account
        response = get_bank_accounts(self, user_id=user_id)
        selected_bank_account = response

        # Display the selected bank account in a dropdown menu
        dashboard_screen_bankaccount_label = Label(dashboard_screen, text="Selected Bank Account: ", fg=PRIMARY_FG, bg=PRIMARY_BG, font=H2 )
        dashboard_screen_bankaccount_label.pack()
        selected_bank_account = StringVar(dashboard_screen)
        selected_bank_account.set(BANKACCOUNTS[0])
        dashboard_screen_bankaccount_entry = OptionMenu(dashboard_screen, selected_bank_account, *BANKACCOUNTS)
        dashboard_screen_bankaccount_entry.pack()

        # Summery button (Not finished)
        WALLET_ICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/wallet.png'))
        WALLET_ICON_LOAD = Image.open(WALLET_ICON_PATH)
        WALLET_ICON = ImageTk.PhotoImage(WALLET_ICON_LOAD)
        Button(
            dashboard_screen,
            image=WALLET_ICON,
            height=150, text="Summary",
            command="",
            background=SECONDARY_BG_BTN,
            fg=SECONDARY_FG,
            font=H3,
            compound=TOP,
            relief=SOLID,
            bd=0
        ).pack(fill=BOTH, padx=10, pady=10, side=TOP, expand=True)

        # Button to open transfer window (in progress)
        TRANSFER_ICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/transfer.png'))
        TRANSFER_ICON_LOAD = Image.open(TRANSFER_ICON_PATH)
        TRANSFER_ICON = ImageTk.PhotoImage(TRANSFER_ICON_LOAD)
        Button(
            dashboard_screen, image=TRANSFER_ICON, height=150, text="Transfer",
            command=lambda : transfer1.transfer(user_id, name, selected_bank_account.get()),
            background=SECONDARY_BG_BTN,
            fg=SECONDARY_FG,
            font=H3,
            compound=TOP,
            relief=SOLID,
            bd=0).pack(fill=BOTH, padx=10, pady=10, side=RIGHT, expand=True)

        # Button for profits (Not finished)
        PROFITS_ICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/profits.png'))
        PROFITS_ICON_LOAD = Image.open(PROFITS_ICON_PATH)
        PROFITS_ICON = ImageTk.PhotoImage(PROFITS_ICON_LOAD)
        Button(
            dashboard_screen, image=PROFITS_ICON, height=150, text="Invest", command=lambda: get_stock_prediction('NFLX'),
            background=SECONDARY_BG_BTN, fg=SECONDARY_FG, font=H3,compound=TOP,relief=SOLID,
            bd=0).pack(fill=BOTH, padx=10, pady=10, side=LEFT, expand=True)

        # For future bitcoin window (Not finished)
        BITCOIN_ICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/bitcoin.png'))
        BITCOIN_ICON_LOAD = Image.open(BITCOIN_ICON_PATH)
        BITCOIN_ICON = ImageTk.PhotoImage(BITCOIN_ICON_LOAD)
        Button(
            dashboard_screen, image=BITCOIN_ICON, height=150, text="Bitcoin", command="",
            background=SECONDARY_BG_BTN, fg=SECONDARY_FG, font=H3, compound=TOP, relief=SOLID,
            bd=0).pack(fill=BOTH, padx=10, pady=10, side=BOTTOM, expand=True)

        # For searching through our bank in the future (Not finished)
        SEARCH_ICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/search.png'))
        SEARCH_ICON_LOAD = Image.open(SEARCH_ICON_PATH)
        SEARCH_ICON = ImageTk.PhotoImage(SEARCH_ICON_LOAD)
        Button(
            dashboard_screen, image=SEARCH_ICON, height=150, text="Search", command="",
            background=SECONDARY_BG_BTN, fg=SECONDARY_FG, font=H3, compound=TOP, relief=SOLID,
            bd=0).pack(fill=BOTH, padx=10, pady=10, ipadx=50, side=LEFT, expand=True)

        # Plans to create a statistic window to view all KPI's for an account
        BARCHART_ICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/barchart.png'))
        BARCHART_ICON_LOAD = Image.open(BARCHART_ICON_PATH)
        BARCHART_ICON = ImageTk.PhotoImage(BARCHART_ICON_LOAD)
        Button(
            dashboard_screen,
            image=BARCHART_ICON, height=150, text="Statistics",command="",
            background=SECONDARY_BG_BTN, fg=SECONDARY_FG, font=H3, compound=TOP,relief=SOLID,
            bd=0).pack(fill=BOTH, padx=10, pady=10, side=TOP, expand=True)

        # Define settings button to call settings window
        SETTINGS_ICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/settings.png'))
        SETTINGS_ICON_LOAD = Image.open(SETTINGS_ICON_PATH)
        SETTINGS_ICON = ImageTk.PhotoImage(SETTINGS_ICON_LOAD)
        Button(
            dashboard_screen,image=SETTINGS_ICON, height=150, text="Settings",
            command=lambda: user1.settings(user_id, name, address, country, city, zipcode, email),
            background=SECONDARY_BG_BTN, fg=SECONDARY_FG, font=H3, compound=TOP, relief=SOLID,
            bd=0).pack(fill=BOTH, padx=10, pady=10, side=RIGHT, expand=True)

        # Store images in variables
        Button.image = WALLET_ICON, BITCOIN_ICON, SEARCH_ICON, BARCHART_ICON, TRANSFER_ICON, PROFITS_ICON, SETTINGS_ICON


    # Designing Main(first) window
    def main_account_screen(self):
        global main_screen
        main_screen = Tk()
        main_screen.geometry(SCREEN_SIZE_LM)
        main_screen.configure(bg=PRIMARY_BG)
        main_screen.iconbitmap(FAVICON_PATH)
        main_screen.title(COMPANY_NAME + " - " + "Client")

        # Logo
        LOGO_LOAD = Image.open(LOGO_PATH)
        LOGO = ImageTk.PhotoImage(LOGO_LOAD)
        Label(image=LOGO, bg=PRIMARY_BG, height=200, width=300).pack()

        # Buttons - Lambda makes sure the function doesn't run on start up
        Button(text="Login", command=lambda: user1.login(), width=19, height=1, font=H3, bg=PRIMARY_BG_BTN,
               fg=SECONDARY_FG, relief=SOLID, bd=0).pack(fill=BOTH, padx=8, pady=10, side=LEFT)
        Button(text="Register", command=lambda: user1.register(), width=19, height=1, font=H3, bg=SECONDARY_BG_BTN,
               fg=SECONDARY_FG, relief=SOLID, bd=0).pack(fill=BOTH, padx=8, pady=10, side=RIGHT)

        # Create logo label
        Label.image = LOGO
        main_screen.mainloop()


# Closing windows
def close_message():
    message_screen.destroy()


def close_create_bank_account_screen():
    create_bank_account_screen.destroy()
    close_message()


def close_settings():
    settings_screen.destroy()


def close_dashboard_screen():
    dashboard_screen.destroy()


def close_register():
    register_screen.destroy()

transfer1 = TransferArg()


# Initiate main screen and create a user object
user1 = UserArg()
user1.main_account_screen()
