# Import packages
try:
    import os  # System functions like get working dir and os.path
    from pathlib import Path  # Deals with file paths on Windows, Mac and Linux
    import requests  # Create http requests
    from tkinter import *
    from PIL import Image, ImageTk
    import json
    from datetime import datetime
except Exception as e:
    print('Some modules are missing {}'.format(e))
# DO NOT MODIFY ABOVE LINE UNLESS YOU KNOW WHAT YOU ARE DOING


# Define global API HTTP hosts
API_USER_SERVICE = 'http://localhost:8000'
API_IMPORT_SERVICE = 'http://localhost:8001'
API_TRANSACTION_SERVICE = 'http://localhost:8002'
API_BANKACCOUNT_SERVICE = 'http://localhost:8003'


# Get current directory - using Path to convert to the appropriate file path (Mac OS X / Windows / Linux)
WORKING_DIR = Path(os.getcwd())
# Favicon file path
FAVICON_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/favicon.ico'))
# Logo file path
LOGO_PATH = os.path.join(os.path.realpath(WORKING_DIR), Path('assets/images/logo.png'))
# Define a variable name for our bank
COMPANY_NAME = "FlexioBank"

# Background colors
PRIMARY_BG = "white"
SECONDARY_BG = "grey4"

# Text colors
PRIMARY_FG = "#006db7"
SECONDARY_FG = "white"
WARNING_FG = "#CE2D4F"

# Button colors
PRIMARY_BG_BTN = "#009DDD"
SECONDARY_BG_BTN = "#006db7"
WARNING_BTN = "#CE2D4F"
MESSAGE_BTN = "#FFB400"
SUCCESS_BTN = "#7FB800"

# Screen size
SCREEN_SIZE_XL = "1200x1200"
SCREEN_SIZE_L = "900x900"
SCREEN_SIZE_LMM = "700x900"
SCREEN_SIZE_LLM = "300x430"
SCREEN_SIZE_LM = "500x300"
SCREEN_SIZE_M = "300x250"
SCREEN_SIZE_S = "400x150"

# Font
PRIMARY_FONT = "Acumin Pro"
SECONDARY_FONT = "Calibri"

# Font size
FONT_SIZE_XL = 32
FONT_SIZE_L = 24
FONT_SIZE_M = 17
FONT_SIZE_S = 12

# Headings
H1 = (PRIMARY_FONT, FONT_SIZE_L)
H2 = (PRIMARY_FONT, FONT_SIZE_M)
H3 = (SECONDARY_FONT, FONT_SIZE_M)
H4 = (SECONDARY_FONT, FONT_SIZE_S)

# Text
PRIMARY_TEXT = (SECONDARY_FONT, FONT_SIZE_S)
