# Import modules
try:
    import json
    from datetime import datetime
    from flask import Flask
    import requests
    from flask_restful import Resource, Api
    from mongoengine import connect
except Exception as e:
    print('Some modules are missing {}'.format(e))


# Import database models
from models import *


# Define external API's to import countries and currencies
COUNTRIES_API_URL = 'https://restcountries.eu/rest/v2/'
CURRENCIES_API_URL = 'https://v6.exchangerate-api.com/v6/b74bd121f9bef7f2c50201ca/latest/DKK'


# Add connection string for MongoDB and load with MongoEngine
connect(host="mongodb://mongodb:27017/FlexioBank")

# Import countries class
class ImportCountries(Resource):
    def get(self):   # GET request
        # Declare variable for countries response
        currency = ''
        # Iterate through objects and if countries exists mark 'found'
        for result in CountryModel.objects:
            currency = 'found'

        # If country exists - do nothing
        if currency == 'found':
            return('Currencies exist')
        else:
            print('No currencies found - importing currencies...')
            # Drop country collection
            CountryModel.drop_collection()
            print('CountryModel dropped')

            # Country ID variable
            country_id_inc = 0

            # Import countries from public API
            response = requests.get(COUNTRIES_API_URL)
            country_json_array = json.loads(response.text)

            # Add record variable
            record_number = 0

            total_records = len(country_json_array)

            # Iterate through json array of countries and assign variables.
            for item in country_json_array:
                country_id_inc = country_id_inc + 1
                country_id = country_id_inc
                country_name = item['name']
                country_iso = item['alpha2Code']
                for currency in item['currencies']:
                    currency_iso = currency['code']
                for currency in item['currencies']:
                    currency_symbol = currency['symbol']
                for currency in item['currencies']:
                    currency_name = currency['name']
                for languages in item['languages']:
                    language = languages['name']
                region = item['region']

                # Map variables with columns in MongoDB
                country = CountryModel(
                        countryId=country_id,
                        countryISO=country_iso,
                        countryName=country_name,
                        currencyISO=currency_iso,
                        currencySymbol=currency_symbol,
                        currencyName=currency_name,
                        language=language,
                        region=region
                    )

                # Output imported records
                record_number = record_number + 1
                print(record_number, '/', total_records, 'countries imported')

                # Update database
                country.save().to_json()
            return('Countries imported')


# Import currencies class
class ImportCurrencies(Resource):
    def get(self):  # GET request
        # Declare variable for countries response
        country = ''
        # Iterate through objects and if countries exists mark 'found'
        for result in CountryModel.objects:
            country = 'found'

        # If country exists - do nothing
        if country == 'found':
            return ('Countries exist')
        else:
            print('No countries found - importing countries...')
            # Clean currency model
            CurrencyModel.drop_collection()

            # Import currencies from public API
            response = requests.get(CURRENCIES_API_URL)

            # Convert to dict
            currency_dict = response.json()

            # Extract rates from dict
            rates = currency_dict['conversion_rates']

            # Set updated time for all currencies
            updated_datetime = datetime.fromtimestamp(currency_dict['time_last_update_unix'])

            # Add record variable
            record_number = 0

            total_records = len(rates.items())

            # Iterate through exchange rates and assign to variables
            for key, value in rates.items():

                # Map keys and values to database model columns
                currency = CurrencyModel(
                        currencyISO=key,
                        currencyISOValue=value,
                        updated_datetime=updated_datetime
                    )
                # Update database
                currency.save().to_json()

                # Output imported records
                record_number = record_number + 1
                print(record_number, '/', total_records, 'currencies imported')

            return "Currencies imported"


# Reset database from user input
class CleanDatabase(Resource):
    def get(self):
        BankAccountModel.drop_collection()
        UserModel.drop_collection()
        TransactionModel.drop_collection()
        return 'Database cleaned'
# Define python API with Flask
app = Flask(__name__)
api = Api(app)


# Define routes
api.add_resource(ImportCountries, '/import_countries')  # Endpoint to import countries
api.add_resource(ImportCurrencies, '/import_currencies')  # Endpoint to import currencies
api.add_resource(CleanDatabase, '/clean_database')  # Resets the database from the user input


# Initialize service
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8001, debug=False)  # run our Flask app
